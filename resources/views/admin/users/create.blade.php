@extends('admin.layout.app')
@section('content')
    @push('css')
    @endpush
    @push('js')
    @endpush
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
            <span class="card-icon">
                <i class="fa fa-user-plus text-primary"></i>
            </span>
                <h3 class="card-label">
                    Admin New Admin
                </h3>
            </div>

        </div>
        <div class="card-body">
            <div>
                <form action="{{route('admin.user.store')}}" method="post">
                    @csrf
                    <div class="card-body">

                        <div class="form-group">
                            <label>Name<span class="text-danger">*</span></label>
                            <input type="name" name="name" class="form-control"  placeholder="Enter Nmae"/>
                        </div>
                        <div class="form-group">
                            <label>Email address <span class="text-danger">*</span></label>
                            <input type="email" name="email" class="form-control"  placeholder="Enter email"/>
                            <span class="form-text text-muted">We'll never share your email with anyone else.</span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password <span class="text-danger">*</span></label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password"/>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Password Confirmation<span class="text-danger">*</span></label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"/>
                        </div>





                    </div>


                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Save</button>
                        <a href="{{route('admin.users')}}" class="btn btn-secondary">Cancel</a>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
