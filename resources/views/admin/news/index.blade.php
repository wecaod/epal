@extends('admin.layout.app')
@section('content')
    @push('css')
    @endpush
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
            <span class="card-icon">
                <i class="fa fa-list-alt text-primary"></i>
            </span>
                <h3 class="card-label">
                    News Management
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="{{route('admin.news.create')}}" class="btn btn-sm btn-light-primary font-weight-bold">
                    <i class="fa fa-plus"></i> ADD NEW NEWS
                </a>
            </div>
        </div>
        <div class="card-body">
            @if(session()->has('success'))

                <div class="alert alert-dismissible bg-success d-flex flex-column flex-sm-row p-5 mb-10">
                    <span class="svg-icon-2hx svg-icon-light me-4 mb-5 mb-sm-0"></span>
                    <div class="d-flex flex-column text-light pe-0 pr-sm-10">
                        <span>{{session()->get('success')}}</span>
                    </div>
                    <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                        <span class="svg-icon svg-icon-2x svg-icon-light"></span>
                    </button>
                </div>
            @endif
            <div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td> صورة الخبر</td>
                        <td>تصنيف الخبر </td>
                        <td>Details News</td>
                        <td>Details News</td>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($news as $key=>$newss)
                        <tr>
                            <td>{{ $key+1}}</td>
                            <td><img src="{{$newss->image_url}}" width="70"/> </td>
                            <td>{{@$newss->category->name}}</td>
                            <td>{{$newss->newsAddress}}</td>
                            <td>{{$newss->newsDetails}}</td>
                            <td>
                                <a href="{{route('admin.news.edit',['news'=>$newss])}}" class="btn btn-icon">
                                    <i class="fa fa-user-edit"></i>
                                </a>





                                <form action="{{route('admin.news.destroy',['news'=>$newss->id])}}" method="post">

                                    @csrf
                                    <button type="submit" class="btn btn-icon red">
                                        <i class="fa fa-trash"></i>
                                    </button>




                                </form>


                            <td/>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row text-center">
                    {{--                    {{$category->links()}}--}}
                </div>
            </div>
        </div>

    </div>
@endsection
{{--@push('js')--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>--}}
{{--    <script type="text/javascript">--}}

{{--        $('.show_confirm').click (function(event) {--}}

{{--            var form = $(this).closest("form");--}}
{{--            var name = $(this).data ("name");--}}

{{--            event.preventDefault();--}}
{{--            swal ({--}}

{{--                title: "Are you sure you want to delete"+name+ '?',--}}
{{--                text: "If you delete this, it will be gone forever.",--}}
{{--                icon: "warning",--}}
{{--                buttons: true,--}}
{{--                dangerMode: true,--}}

{{--            })--}}

{{--                .then((willDelete) => {--}}

{{--                    if (willDelete) {--}}
{{--                        form.submit();--}}

{{--                    }--}}



{{--                });--}}

{{--        });--}}

{{--    </script>--}}
{{--@endpush--}}
