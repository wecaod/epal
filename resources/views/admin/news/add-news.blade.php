@extends('admin.layout.app')
@section('content')
    @push('css')
    @endpush
    @push('js')
    @endpush
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">
                Add News
            </h3>
        </div>
        <!--begin::Form-->
        <form class="form" action="{{route('admin.news.store')}}" method="post" enctype='multipart/form-data'>
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label class="">صورة الخبر</label>
                    <input type="file" name="image_file" accept="image/jpg,image/png,image/jpeg,image/svg"/>

                </div>

                <div class="form-group">
                    <label class=""> تصنيف الخبر</label>
                    <select name="cat_id" class="form-control">
                        @foreach($categories as $cat)
                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>News Address</label>
                    <input type="text" name="newsAddress" class="form-control form-control-lg"  placeholder="News Address"/>
                    <label>News Details</label>
                    <textarea type="text" name="newsDetails" class="form-control form-control-lg"  placeholder="News Details"/></textarea>
                </div>


                <div class="card-footer">
                    <button type="submit" class="btn btn-success mr-2">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div
    >@endsection

