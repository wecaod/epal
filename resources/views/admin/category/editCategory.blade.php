@extends('admin.layout.app')
@section('content')
    @push('css')
    @endpush
    @push('js')
    @endpush
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">
                Add Category
            </h3>
        </div>
        <!--begin::Form-->
        <form class="form" action="{{route('admin.category.update',['category'=>$category])}}" method="post">
            @csrf
            <div class="card-body">

                <div class="form-group">
                    <label>Name Of Category</label>
                    <input type="text" name="name" class="form-control form-control-lg"  placeholder="category name" value="{{@$categoryx->name}}"/>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success mr-2">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>@endsection

