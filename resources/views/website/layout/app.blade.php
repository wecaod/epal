<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>News HTML-5 Template </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('website/img/favicon.ico')}}">

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('website/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/ticker-style.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('website/css/style.css')}}">
    @stack('css')
</head>

<body>

<!-- Preloader Start -->
<!-- <div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
                <img src="{{asset('website/img/logo/logo.png')}}')}}" alt="">
            </div>
        </div>
    </div>
</div> -->
<!-- Preloader Start -->

@include('website.layout.header')
@yield('content')
@include('website.layout.footer')
<!-- JS here -->

<!-- All JS Custom Plugins Link Here here -->
<script src="./{{asset('website/js/vendor/modernizr-3.5.0.min.js')}}"></script>
<!-- Jquery, Popper, Bootstrap -->
<script src="./{{asset('website/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="./{{asset('website/js/popper.min.js')}}"></script>
<script src="./{{asset('website/js/bootstrap.min.js')}}"></script>
<!-- Jquery Mobile Menu -->
<script src="./{{asset('website/js/jquery.slicknav.min.js')}}"></script>

<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="./{{asset('website/js/owl.carousel.min.js')}}"></script>
<script src="./{{asset('website/js/slick.min.js')}}"></script>
<!-- Date Picker -->
<script src="./{{asset('website/js/gijgo.min.js')}}"></script>
<!-- One Page, Animated-HeadLin -->
<script src="./{{asset('website/js/wow.min.js')}}"></script>
<script src="./{{asset('website/js/animated.headline.js')}}"></script>
<script src="./{{asset('website/js/jquery.magnific-popup.js')}}"></script>

<!-- Breaking New Pluging -->
<script src="./{{asset('website/js/jquery.ticker.js')}}"></script>
<script src="./{{asset('website/js/site.js')}}"></script>

<!-- Scrollup, nice-select, sticky -->
<script src="./{{asset('website/js/jquery.scrollUp.min.js')}}"></script>
<script src="./{{asset('website/js/jquery.nice-select.min.js')}}"></script>
<script src="./{{asset('website/js/jquery.sticky.js')}}"></script>

<!-- contact js -->
<script src="./{{asset('website/js/contact.js')}}"></script>
<script src="./{{asset('website/js/jquery.form.js')}}"></script>
<script src="./{{asset('website/js/jquery.validate.min.js')}}"></script>
<script src="./{{asset('website/js/mail-script.js')}}"></script>
<script src="./{{asset('website/js/jquery.ajaxchimp.min.js')}}"></script>

<!-- Jquery Plugins, main Jquery -->
<script src="./{{asset('website/js/plugins.js')}}')}}"></script>
<script src="./{{asset('website/js/main.js')}}"></script>
@stack('js')

</body>
</html>
