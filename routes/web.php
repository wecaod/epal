<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[\App\Http\Controllers\websiteController::class,'news'])->name('news');
Route::get('/new-by-cat/{cat}',[\App\Http\Controllers\websiteController::class,'newsByCategory'])->name('get_news_by_cat');
Route::get('/test',[\App\Http\Controllers\Admin\Homecotroller::class,'welcome']);
Route::get('/about',[\App\Http\Controllers\websiteController::class,'about'])->name('website.about');


Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>'auth'],function (){
    Route::group(['prefix'=>'manage'],function(){
        Route::get('/',[\App\Http\Controllers\Admin\AdminController::class,'index'])->name('users');
        Route::get('/create',[\App\Http\Controllers\Admin\AdminController::class,'create'])->name('create.user');
        Route::get('/edit/{admin}',[\App\Http\Controllers\Admin\AdminController::class,'edit'])->name('edit.user');
        Route::post('/update/{admin}',[\App\Http\Controllers\Admin\AdminController::class,'update'])->name('update.user');
        Route::post('/store',[\App\Http\Controllers\Admin\AdminController::class,'store'])->name('user.store');
        Route::post('/destroy/{admin}',[\App\Http\Controllers\Admin\AdminController::class,'destroy'])->name('user.destroy');
    });
    Route::group(['prefix'=>'category'],function(){
        Route::get('/',[\App\Http\Controllers\Admin\CategoryController::class,'index'])->name('category');
        Route::get('/create',[\App\Http\Controllers\Admin\CategoryController::class,'create'])->name('category.create');
        Route::post('/store',[\App\Http\Controllers\Admin\CategoryController::class,'store'])->name('category.store');
        Route::get('/edit/{category}',[\App\Http\Controllers\Admin\CategoryController::class,'edit'])->name('category.edit');
        Route::post('/update/{category}',[\App\Http\Controllers\Admin\CategoryController::class,'update'])->name('category.update');
        Route::post('/destroy/{category}',[\App\Http\Controllers\Admin\categoryController::class,'destroy'])->name('category.destroy');

    });
    Route::group(['prefix'=>'news'],function (){
        Route::get('/',[\App\Http\Controllers\Admin\NewsController::class,'index'])->name('news');

        Route::get('/create',[\App\Http\Controllers\Admin\NewsController::class,'create'])->name('news.create');
        Route::post('/store',[\App\Http\Controllers\Admin\NewsController::class,'store'])->name('news.store');
        Route::get('/edit/{news}',[\App\Http\Controllers\Admin\NewsController::class,'edit'])->name('news.edit');
        Route::post('/update/{news}',[\App\Http\Controllers\Admin\NewsController::class,'update'])->name('news.update');
        Route::post('/destroy/{news}',[\App\Http\Controllers\Admin\NewsController::class,'destroy'])->name('news.destroy');
    });
    Route::get('/home',[\App\Http\Controllers\Admin\Homecotroller::class,'admin'])->name('admin');
});

Auth::routes(['register'=>false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('admin');

