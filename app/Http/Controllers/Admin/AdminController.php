<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;

class AdminController extends Controller
{
    public function index(){
        $users=User::orderBy('created_at','desc')->paginate(10);
        $data=[
            'users'=>$users
        ];
    return view('admin.users.index',$data);
    }
    public function create(Request $request){

        return view('admin.users.create');

    }
    public function store(Request $request){
        $user = new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=Hash::make($request->password);
        $user->save();
        return redirect()->to(route('admin.users'))->with('success','تم إضافة المستخدم بنجاح');
    }
    public function edit(Request $request,User $admin){

        return view('admin.users.edit',compact('admin'));

    }
    public function update(Request $request,User $admin){
        $request->validate([
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'nullable',
        ]);
        $admin->name=$request->name;
        $admin->email=$request->email;
        if($request->filled('password')){

            $admin->password=Hash::make($request->password);
        }
        $admin->save();
        return redirect()->to(route('admin.users'))->with('success','تم تحديث بيانات المستخدم بنجاح');
    }
    public function destroy(Request $request,$admin){
        $user=User::find($admin);
        if ($user){
            $user->delete();
        }
        return redirect()->to(route('admin.users'))->with('success','تم حذف بيانات المستخدم بنجاح');

    }

}
