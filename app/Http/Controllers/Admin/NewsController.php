<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use Illuminate\Http\Request;
use File;
class NewsController extends Controller
{
    public function index(){
        $news=News::get();
        $data=[
            'news'=>$news
        ];
        return view('admin.news.index',$data);
    }

    public function create(Request $request){

            $categories=Category::get();
            $data=['categories'=>$categories];
        return view('admin.news.add-news',$data);

    }
    public function store(Request $request){

//        $request->validate([
//
//            'image_file'=>'nullable|image'
//        ]);

        $imageName=null;

        if($request->hasFile('image_file')){

            $imageName = rand(0,9999).time().'.'.$request->image_file->extension();
            $path=public_path('imgs');
            if(!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
                // path does not exist
            }
            $request->image_file->move($path, $imageName);
        }

        News::create([
            'newsAddress'=>$request->newsAddress,
            'newsDetails'=>$request->newsDetails,
            'main_image'=>$imageName,
            'cat_id'=>(int)$request->cat_id,
        ]);

//        $news = new News();
//        $news->newsAddress=$request->newsAddress;
//        $news->newsDetails=$request->newsDetails;
//        $news->save();
        return redirect()->to(route('admin.news'))->with('success','تم إضافة الخبر بنجاح');
    }
    public function edit(Request $request,News $news){
        $categories=Category::get();

        return view('admin.news.editNews',compact('news','categories'));

    }
    public function update(Request $request,News $news){
//        $request->validate([
//            'newsAddress'=>'required',
//            'newsDetails'=>'required',
////            'cat_id'=>'required',
//        ]);
        $news->newsAddress=$request->newsAddress;
        $news->newsDetails=$request->newsDetails;
        $news->cat_id=$request->cat_id;
        $main_image=$news->main_image;

        if($request->hasFile('image_file')){

            $imageName = rand(0,9999).time().'.'.$request->image_file->extension();
            $path=public_path('imgs');
            if(!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
                // path does not exist
            }
            $request->image_file->move($path, $imageName);
            $news->main_image=$imageName;
        }
        $news->save();
//        if($request->hasFile('image_file')){
////            unlink(asset('ímgs/'.$main_image));
//        }
        return redirect()->to(route('admin.news'))->with('success','تم تحديث الخبر بنجاح');
    }
    public function destroy(Request $request,$news){
        $news=News::find($news);
        if ($news){
            $news->delete();
        }
        return redirect()->to(route('admin.news'))->with('success','تم حذف الخبر بنجاح');

    }
}
