<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class categoryController extends Controller
{
public function index(){
        $category=Category::all();
    $data=[
        'category'=>$category
    ];
    return view('admin.category.index',$data);
}

    public function create(Request $request){

        return view('admin.category.addCategory');

    }
    public function store(Request $request){
        $category = new Category();
        $category->name=$request->name;
        $category->save();
        return redirect()->to(route('admin.category'))->with('success','تم إضافة المنتج بنجاح');
    }
    public function edit(Request $request,Category $category){

        return view('admin.category.editCategory',compact('category'));

    }
    public function update(Request $request,Category $category){
        $request->validate([
            'name'=>'required',
        ]);
        $category->name=$request->name;
        $category->save();
        return redirect()->to(route('admin.category'))->with('success','تم تحديث اسم بنجاح');
    }
    public function destroy(Request $request,$category){
        $category=Category::find($category);
        if ($category){
            $category->delete();
        }
        return redirect()->to(route('admin.category'))->with('success','تم حذف المنتج بنجاح');

    }
}
