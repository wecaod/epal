<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\News;
use Illuminate\Http\Request;

class websiteController extends Controller
{
    public function news(Request $request){

        $news=News::whereNotNull('main_image')->get();
        $categories=Category::whereHas('news')->get();
//        $news=News::get();
//        return $news;
        $data=[
            'news'=>$news,
            'categories'=>$categories,
        ];

        return view('news',$data);

    }
    public function newsByCategory(Request $request,$cat){

        $news=News::whereNotNull('main_image')->where('cat_id',$cat)->get();
        $categories=Category::whereHas('news')->get();
//        $news=News::get();
//        return $news;
        $data=[
            'news'=>$news,
            'categories'=>$categories,
        ];

        return view('news_by_cat',$data);

    }
    public function about(){
        return view('website.about');
    }
}
