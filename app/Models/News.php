<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    protected $fillable = [
        'newsAddress',
        'newsDetails',
        'main_image',
        'cat_id',
    ];
    protected $appends=['image_url'];

    public function getImageUrlAttribute()
    {
        if($this->main_image){
            return asset('imgs/'.$this->main_image);
        }
        return null;
    }

   public function category(){
        return $this->belongsTo(Category::class,'cat_id');
   }
}
